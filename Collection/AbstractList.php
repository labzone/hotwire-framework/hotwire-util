<?php

namespace HotWire\Util\Collection;

abstract class AbstractList
{
    protected $items;

    public function __construct(array $items=array())
    {
        $this->items=$items;
    }

    public function count()
    {
        return count($this->items);
    }

    public function append($item)
    {
        $this->items[]=$item;

        return $this;
    }

    public function remove($item)
    {
        if ($key=array_search($item, $this->items)) {
            unset($this->items[$key]);

            return true;
        }

        return false;
    }

    public function get($key, $default=null)
    {
        if (is_int($key)) {
            $keys = array_keys($this->items);
            if (isset($keys[$key])) {
                return htmlspecialchars($this->items[$keys[$key]], ENT_QUOTES, 'UTF-8');
            }
        } else {
            if (isset($this->items[$key])) {
                return htmlspecialchars($this->items[$key], ENT_QUOTES, 'UTF-8');
            }
        }

        return $default;
    }

    public function hasItem()
    {
        return $this->count() > 0;
    }

    public function getItems()
    {
        return $this->items;
    }

    abstract public function createIterator();
}
