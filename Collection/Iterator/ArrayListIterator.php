<?php

namespace HotWire\Util\Collection\Iterator;

use HotWire\Util\Collection\AbstractList;

class ArrayListIterator implements \Iterator
{
    protected $list;
    protected $key;

    public function __construct(AbstractList $list)
    {
        $this->list=$list;
        $this->key=0;
    }

    public function key()
    {
        return $this->key;
    }

    public function next()
    {
        return ++$this->key;
    }

    public function current()
    {
        return $this->list->get($this->key);
    }

    public function rewind()
    {
        $this->key=0;

        return $this;
    }

    public function valid()
    {
        return isset($this->list[$this->key]);
    }
}
