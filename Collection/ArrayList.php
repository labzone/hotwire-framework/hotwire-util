<?php

namespace HotWire\Util\Collection;

use HotWire\Util\Collection\Iterator\ArrayListIterator;

class ArrayList extends AbstractList
{

    public function __call($name, $arguments)
    {
        $match=null;
        if (preg_match('/^get([A-Za-z]+)/', $name, $match)) {
            if ($item=end($match)) {
                return $this->get(strtolower($item));
            }
        }

        throw new \Exception("Item not found");
    }

    public function createIterator()
    {
        return new ArrayListIterator($this);
    }
}
