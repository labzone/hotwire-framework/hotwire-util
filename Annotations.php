<?php

namespace HotWire\Util;

class Annotations
{
    public static function getClass($type)
    {
        $reflectionClass=new \ReflectionClass($type);

        return str_replace('use ',null,self::getComment($reflectionClass->getDocComment()));
    }

    public static function getMethods($type)
    {
        $properties=array();
        $reflectionClass=new \ReflectionClass($type);
        foreach ($reflectionClass->getProperties() as $propertyInfo) {
            $comment=self::getComment($propertyInfo->getDocComment());
            $properties[$propertyInfo->getName()]=array(
                'method'=>self::getMethodName($comment),
                'arguments'=>self::getArguments($comment)
            );
        }

        return $properties;
    }

    private static function getArguments($name)
    {
        $match=array();
        preg_match('/[a-zA-Z]+\(([a-zA-Z0-9,:]+)\)/', $name, $match);

        return array_filter(explode(',',end($match)));
    }

    private static function getMethodName($name)
    {
        $match=array();
        preg_match('/([a-zA-Z]+)\(?.*\)?/', $name, $match);

        return end($match);
    }

    private static function getComment($docComment)
    {
        $docComment = str_replace('/*', '', $docComment);
        $docComment = str_replace('*/', '', $docComment);
        $docComment = str_replace('*', '', $docComment);
        $comment = explode('@', $docComment);
        if (is_array($comment) && count($comment) > 0) {
            return trim($comment[1]);
        }

        return null;
    }
}
